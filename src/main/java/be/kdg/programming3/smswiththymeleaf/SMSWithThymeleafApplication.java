package be.kdg.programming3.smswiththymeleaf;

import be.kdg.programming3.smswiththymeleaf.service.StudentService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class SMSWithThymeleafApplication {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(SMSWithThymeleafApplication.class, args);
        context.getBean(StudentService.class).getStudents().forEach(System.out::println);
    }

}
