package be.kdg.programming3.smswiththymeleaf.configuration;

import be.kdg.programming3.smswiththymeleaf.domain.Student;
import be.kdg.programming3.smswiththymeleaf.repository.StudentRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Random;
import java.util.stream.Stream;

@Component
public class DatabaseSeeder implements CommandLineRunner {
    private StudentRepository studentRepository;

    public DatabaseSeeder(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        Random random = new Random();
        Stream.generate(() -> new Student("student" + random.nextInt(100),
                        LocalDate.of(random.nextInt(1970,2010),random.nextInt(1,13), random.nextInt(1,28)) ))
                .limit(15).forEach(s -> studentRepository.createStudent(s));
    }
}
