package be.kdg.programming3.smswiththymeleaf.presentation;

import be.kdg.programming3.smswiththymeleaf.domain.Student;
import be.kdg.programming3.smswiththymeleaf.domain.User;
import be.kdg.programming3.smswiththymeleaf.service.StudentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/students")
public class StudentController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping
    public String showStudentsView(Model model){
        logger.info("controller is running showStudentsView!");
        List<Student> students = studentService.getStudents();
        model.addAttribute("students", students);
        model.addAttribute("user", new User("Joske","Vermeulen"));
        return "allstudents";
    }
}
