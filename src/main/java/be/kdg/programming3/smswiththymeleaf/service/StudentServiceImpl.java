package be.kdg.programming3.smswiththymeleaf.service;

import be.kdg.programming3.smswiththymeleaf.domain.Student;
import be.kdg.programming3.smswiththymeleaf.repository.StudentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class StudentServiceImpl implements StudentService{
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private StudentRepository studentRepository;

    public StudentServiceImpl(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    public List<Student> getStudents() {
        logger.info("Getting students...");
        return studentRepository.readStudents();
    }

}
