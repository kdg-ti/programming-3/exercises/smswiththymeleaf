package be.kdg.programming3.smswiththymeleaf.service;

import be.kdg.programming3.smswiththymeleaf.domain.Student;

import java.util.List;

public interface StudentService {
    List<Student> getStudents();
}
