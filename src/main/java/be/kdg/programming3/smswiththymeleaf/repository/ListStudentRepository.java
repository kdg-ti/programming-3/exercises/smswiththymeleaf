package be.kdg.programming3.smswiththymeleaf.repository;

import be.kdg.programming3.smswiththymeleaf.domain.Student;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ListStudentRepository implements StudentRepository {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private static List<Student> students = new ArrayList<>();

    @Override
    public List<Student> readStudents() {
        logger.info("Reading students from database");
        return students;
    }

    @Override
    public Student createStudent(Student student) {
        student.setId(students.size());
        students.add(student);
        return student;
    }
}
