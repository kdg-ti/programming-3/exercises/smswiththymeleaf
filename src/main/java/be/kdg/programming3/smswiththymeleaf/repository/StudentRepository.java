package be.kdg.programming3.smswiththymeleaf.repository;

import be.kdg.programming3.smswiththymeleaf.domain.Student;

import java.util.List;

public interface StudentRepository {
    List<Student> readStudents();
    Student createStudent(Student student);
}
